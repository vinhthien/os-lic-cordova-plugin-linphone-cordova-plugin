//
//  TTLinphoneAddress.h
//  HelloCordova
//
//  Created by Thien on 5/14/16.
//
//

#import <Foundation/Foundation.h>
#include "linphone/linphonecore.h"

@interface TTLinphoneAddress : NSObject
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* domain;

-(void)parseLinphoneAddress:(LinphoneAddress*)linphoneAddress;

@end
