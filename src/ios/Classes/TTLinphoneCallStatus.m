//
//  TTLinphoneCallStatus.m
//  HelloCordova
//
//  Created by Thien on 5/14/16.
//
//

#import "TTLinphoneCallStatus.h"


@implementation TTLinphoneCallStatus
@synthesize callStatus;

-(void)parseLinphoneCallStatus:(LinphoneCallStatus)linphoneCallStatus {
    if (linphoneCallStatus == LinphoneCallSuccess) {
        callStatus = @"CallSuccess";
    } else if (linphoneCallStatus == LinphoneCallAborted) {
        callStatus = @"CallSuccess";
    } else if (linphoneCallStatus == LinphoneCallMissed) {
        callStatus = @"CallMissed";
    } else if (linphoneCallStatus == LinphoneCallDeclined) {
        callStatus = @"CallDeclined";
    }
}

@end
