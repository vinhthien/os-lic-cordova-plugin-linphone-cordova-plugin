//
//  LinphoneHelper.h
//  HelloCordova
//
//  Created by Apple on 4/23/16.
//
//

#import <Foundation/Foundation.h>

@interface LinphoneHelper : NSObject

+ (LinphoneHelper*)instance;

- (void) initLinphoneCore;
- (void) destroyLinphoneCore;
- (void) registerSIPWithUsername:(NSString*)username displayName:(NSString*)displayName domain:(NSString*)domain password:(NSString*)password transport:(NSString*)transport;
- (void) deregisterSIPWithUsername:(NSString*)username domain:(NSString*)domain;
- (bool) getRegisterStatusSIPWithUsername:(NSString*)username domain:(NSString*)domain;
- (void) makeCallWithUsername:(NSString*)username domain:(NSString*)domain displayName:(NSString*)displayName;
- (void) acceptCall;
- (void) declineCall;
- (void) sendDtmfWithKeyCode:(NSString*)keyCode;
- (int) getVolumeMax;
- (void) volume:(int)volume;
- (void) terminateCall;
- (void) muteCall;
- (void) unmuteCall;
- (void) enableSpeaker;
- (void) disableSpeaker;
- (void) holdCall;
- (void) unholdCall;
- (NSDictionary*) getMissedCallLogs;
- (NSDictionary*) getAllCallLogs;
- (void) deleteAllCallLogs;
- (void) startListener;

@end
