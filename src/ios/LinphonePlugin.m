#import "LinphonePlugin.h"
#import "LinphoneHelper.h"
#import "LinphoneManager.h"


@implementation LinphonePlugin


- (void)initLinphoneCore:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) destroyLinphoneCore:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] destroyLinphoneCore];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) registerSIP:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* displayName = [[command arguments] objectAtIndex:1];
    NSString* domain = [[command arguments] objectAtIndex:2];
    NSString* password = [[command arguments] objectAtIndex:3];
    NSString* transport = [[command arguments] objectAtIndex:4];
    
    [[LinphoneHelper instance] registerSIPWithUsername:username displayName:displayName domain:domain password:password transport:transport];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) deregisterSIP:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    
    [[LinphoneHelper instance] deregisterSIPWithUsername:username domain:domain];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) getRegisterStatusSIP:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    
    CDVPluginResult* pluginResult = nil;
    if ([[LinphoneHelper instance] getRegisterStatusSIPWithUsername:username domain:domain]) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"RegistrationOk"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"RegistrationFailed"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) makeCall:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    NSString* displayName = [[command arguments] objectAtIndex:2];
    
    [[LinphoneHelper instance] makeCallWithUsername:username domain:domain displayName:displayName];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) acceptCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] acceptCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) declineCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] declineCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) sendDtmf:(CDVInvokedUrlCommand*)command {
    NSString* keyCode = [[command arguments] objectAtIndex:0];
    
    if (keyCode && keyCode.length > 0) {
        [[LinphoneHelper instance] sendDtmfWithKeyCode:[keyCode substringFromIndex:0]];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"keyCode must be not empty."];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void) getVolumeMax:(CDVInvokedUrlCommand*)command {
    int volumeMax = [[LinphoneHelper instance] getVolumeMax];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:volumeMax];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) volume:(CDVInvokedUrlCommand*)command {
    NSNumber* volume = [[command arguments] objectAtIndex:0];
    
    [[LinphoneHelper instance] volume:[volume intValue]];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) terminateCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] terminateCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) muteCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] muteCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) unmuteCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] unmuteCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) enableSpeaker:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] enableSpeaker];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) disableSpeaker:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] disableSpeaker];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) holdCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] holdCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) unholdCall:(CDVInvokedUrlCommand*)command {
    [[LinphoneHelper instance] unholdCall];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) startListener:(CDVInvokedUrlCommand*)command {
    [[NSNotificationCenter defaultCenter] addObserverForName:kLinphoneRegistrationUpdate object:nil queue:nil usingBlock:^(NSNotification * _Nonnull notif) {
        
        NSString* message = [notif.userInfo objectForKey:@"message"];
        NSString* state = @"";
        NSNumber *number = [notif.userInfo objectForKey:@"state"];
        LinphoneRegistrationState registrationState = [number integerValue];
        if (registrationState == LinphoneRegistrationOk) {
            state = @"RegistrationOk";
        } else if (registrationState == LinphoneRegistrationNone) {
            state = @"RegistrationNone";
        } else if (registrationState == LinphoneRegistrationProgress) {
            state = @"RegistrationProgress";
        } else if (registrationState == LinphoneRegistrationCleared) {
            state = @"RegistrationCleared";
        } else if (registrationState == LinphoneRegistrationFailed) {
            state = @"RegistrationFailed";
        }
        
        
        NSString* username = @"";
        NSString* domain = @"";
        
        NSValue* cfg = [notif.userInfo objectForKey:@"cfg"];
        LinphoneProxyConfig* proxy = [cfg pointerValue];
        if (proxy != NULL) {
            const LinphoneAuthInfo *auth = linphone_proxy_config_find_auth_info(proxy);
            if (auth) {
                username = [NSString stringWithUTF8String:linphone_auth_info_get_username(auth)];
                domain = [NSString stringWithUTF8String:linphone_auth_info_get_domain(auth)];
            }
        }
        
        
        NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                        initWithObjectsAndKeys :
                                        @"REGISTRATION_CHANGE", @"event",
                                        message, @"message",
                                        username, @"username",
                                        domain, @"domain",
                                        state, @"state",
                                        nil
                                        ];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kLinphoneCallUpdate object:nil queue:nil usingBlock:^(NSNotification * _Nonnull notif) {
        
        
        NSString* message = [notif.userInfo objectForKey:@"message"];
        NSString* state = @"";
        NSNumber *number = [notif.userInfo objectForKey:@"state"];
        LinphoneCallState callState = [number integerValue];
        if (callState == LinphoneCallIdle) {
            state = @"Idle";
        } else if (callState == LinphoneCallIncomingReceived) {
            state = @"IncomingReceived";
        } else if (callState == LinphoneCallOutgoingInit) {
            state = @"OutgoingInit";
        } else if (callState == LinphoneCallOutgoingProgress) {
            state = @"OutgoingProgress";
        } else if (callState == LinphoneCallOutgoingRinging) {
            state = @"OutgoingRinging";
        } else if (callState == LinphoneCallOutgoingEarlyMedia) {
            state = @"OutgoingEarlyMedia";
        } else if (callState == LinphoneCallConnected) {
            state = @"Connected";
        } else if (callState == LinphoneCallStreamsRunning) {
            state = @"StreamsRunning";
        } else if (callState == LinphoneCallPausing) {
            state = @"Pausing";
        } else if (callState == LinphoneCallPaused) {
            state = @"Paused";
        } else if (callState == LinphoneCallResuming) {
            state = @"Resuming";
        } else if (callState == LinphoneCallRefered) {
            state = @"Refered";
        } else if (callState == LinphoneCallError) {
            state = @"OutgoingRinging";
        } else if (callState == LinphoneCallEnd) {
            state = @"CallEnd";
        } else if (callState == LinphoneCallPausedByRemote) {
            state = @"PausedByRemote";
        } else if (callState == LinphoneCallUpdatedByRemote) {
            state = @"UpdatedByRemote";
        } else if (callState == LinphoneCallIncomingEarlyMedia) {
            state = @"IncomingEarlyMedia";
        } else if (callState == LinphoneCallUpdating) {
            state = @"Updating";
        } else if (callState == LinphoneCallReleased) {
            state = @"Released";
        } else if (callState == LinphoneCallEarlyUpdatedByRemote) {
            state = @"EarlyUpdatedByRemote";
        } else if (callState == LinphoneCallEarlyUpdating) {
            state = @"EarlyUpdating";
        }
        
        NSString* caller = @"";
        NSString* callee = @"";
        
        NSValue* callValue = [notif.userInfo objectForKey:@"call"];
        LinphoneCall* call = [callValue pointerValue];
        if (call) {
            LinphoneCallLog *callLog = linphone_call_get_call_log(call);
            if (callLog) {
                LinphoneAddress* callerAddress = linphone_call_log_get_from_address(callLog);
                if (callerAddress) {
                    const char *username = linphone_address_get_username(callerAddress);
                    caller = [NSString stringWithUTF8String:username];
                }
                LinphoneAddress* calleeAddress = linphone_call_log_get_to_address(callLog);
                if (calleeAddress) {
                    const char *username = linphone_address_get_username(calleeAddress);
                    callee = [NSString stringWithUTF8String:username];
                }
            }
        }
        NSString* event = @"CALL_EVENT";
        if (callState == LinphoneCallIncomingReceived) {
            event = @"";
        }
        
        NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                        initWithObjectsAndKeys :
                                        event, @"event",
                                        message, @"message",
                                        caller, @"caller",
                                        callee, @"callee",
                                        state, @"state",
                                        nil
                                        ];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
        [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end