//
//  TTLinphoneCallDir.m
//  HelloCordova
//
//  Created by Thien on 5/17/16.
//
//

#import "TTLinphoneCallDir.h"

@implementation TTLinphoneCallDir

@synthesize callDir;

-(void)parseLinphoneCallDirection:(LinphoneCallDir)linphoneCallDir {
    if (linphoneCallDir == LinphoneCallOutgoing) {
        callDir = @"CallOutgoing";
    } else if (linphoneCallDir == LinphoneCallIncoming) {
        callDir = @"CallIncoming";
    }
}

@end
