#import <Cordova/CDV.h>

@interface LinphonePlugin : CDVPlugin

- (void) initLinphoneCore:(CDVInvokedUrlCommand*)command;
- (void) destroyLinphoneCore:(CDVInvokedUrlCommand*)command;
- (void) registerSIP:(CDVInvokedUrlCommand*)command;
- (void) deregisterSIP:(CDVInvokedUrlCommand*)command;
- (void) getRegisterStatusSIP:(CDVInvokedUrlCommand*)command;
- (void) makeCall:(CDVInvokedUrlCommand*)command;
- (void) acceptCall:(CDVInvokedUrlCommand*)command;
- (void) declineCall:(CDVInvokedUrlCommand*)command;
- (void) sendDtmf:(CDVInvokedUrlCommand*)command;
- (void) getVolumeMax:(CDVInvokedUrlCommand*)command;
- (void) volume:(CDVInvokedUrlCommand*)command;
- (void) terminateCall:(CDVInvokedUrlCommand*)command;
- (void) muteCall:(CDVInvokedUrlCommand*)command;
- (void) unmuteCall:(CDVInvokedUrlCommand*)command;
- (void) enableSpeaker:(CDVInvokedUrlCommand*)command;
- (void) disableSpeaker:(CDVInvokedUrlCommand*)command;
- (void) holdCall:(CDVInvokedUrlCommand*)command;
- (void) unholdCall:(CDVInvokedUrlCommand*)command;
- (void) startListener:(CDVInvokedUrlCommand*)command;

@end