//
//  LinphoneHelper.m
//  HelloCordova
//
//  Created by Apple on 4/23/16.
//
//

#import "LinphoneHelper.h"
#import "TTCallLog.h"
#import "Utils.h"

static LinphoneHelper *theLinphoneHelper = nil;

@implementation LinphoneHelper

+ (LinphoneHelper *)instance {
    @synchronized(self) {
        if (theLinphoneHelper == nil) {
            theLinphoneHelper = [[LinphoneHelper alloc] init];
        }
    }
    return theLinphoneHelper;
}

- (void) initLinphoneCore {
    [[LinphoneManager instance] startLinphoneCore];
}
- (void) destroyLinphoneCore {
    [[LinphoneManager instance] destroyLinphoneCore];
}
- (void) registerSIPWithUsername:(NSString*)username displayName:(NSString*)displayName domain:(NSString*)domain password:(NSString*)password transport:(NSString*)transport {
    LinphoneCore* lc = [LinphoneManager getLc];
    LinphoneProxyConfig* proxyCfg = linphone_core_create_proxy_config(lc);
    
    char normalizedUserName[256];
    linphone_proxy_config_normalize_number(proxyCfg, [username cStringUsingEncoding:[NSString defaultCStringEncoding]], normalizedUserName, sizeof(normalizedUserName));
    
    const char* identity = linphone_proxy_config_get_identity(proxyCfg);
    if( !identity || !*identity ) identity = "sip:user@example.com";
    
    LinphoneAddress* linphoneAddress = linphone_address_new(identity);
    linphone_address_set_username(linphoneAddress, normalizedUserName);
    
    if( domain && [domain length] != 0) {
        // when the domain is specified (for external login), take it as the server address
        linphone_proxy_config_set_server_addr(proxyCfg, [domain UTF8String]);
        linphone_address_set_domain(linphoneAddress, [domain UTF8String]);
    }
    
    identity = linphone_address_as_string_uri_only(linphoneAddress);
    
    linphone_proxy_config_set_identity(proxyCfg, identity);
    
    
    
    LinphoneAuthInfo* info = linphone_auth_info_new([username UTF8String]
                                                    , NULL, [password UTF8String]
                                                    , NULL
                                                    , NULL
                                                    ,linphone_proxy_config_get_domain(proxyCfg));
    
    linphone_proxy_config_enable_register(proxyCfg, true);
    linphone_core_add_auth_info(lc, info);
    linphone_core_add_proxy_config(lc, proxyCfg);
    linphone_core_set_default_proxy_config(lc, proxyCfg);
}

- (void) deregisterSIPWithUsername:(NSString*)username domain:(NSString*)domain {
    // remove previous proxy config, if any
    
    NSMutableArray *new_configs = [LinphoneHelper findAuthIndexOf:username domain:domain];
    for (int index = 0; index < new_configs.count; index++) {
        const MSList *proxyConfigList = linphone_core_get_proxy_config_list(LC);
        NSNumber* number = (NSNumber*)[new_configs objectAtIndex:index];
        LinphoneProxyConfig *new_config = ms_list_nth_data(proxyConfigList, [number integerValue]);
        if (new_config != NULL) {
            const LinphoneAuthInfo *auth = linphone_proxy_config_find_auth_info(new_config);
            linphone_core_remove_proxy_config(LC, new_config);
            if (auth) {
                linphone_core_remove_auth_info(LC, auth);
            }
        }
    }
    
}
- (bool) getRegisterStatusSIPWithUsername:(NSString*)username domain:(NSString*)domain {
    NSMutableArray *new_configs = [LinphoneHelper findAuthIndexOf:username domain:domain];
    for (int index = 0; index < new_configs.count; index++) {
        const MSList *proxyConfigList = linphone_core_get_proxy_config_list(LC);
        NSNumber* number = (NSNumber*)[new_configs objectAtIndex:index];
        LinphoneProxyConfig *new_config = ms_list_nth_data(proxyConfigList, [number integerValue]);
        if (new_config != NULL) {
            return linphone_proxy_config_is_registered(new_config);
        }
    }
    return FALSE;
}
- (void) makeCallWithUsername:(NSString*)username domain:(NSString*)domain displayName:(NSString*)displayName {
    NSString *address = [NSString stringWithFormat:@"%@@%@", username, domain];
    LinphoneAddress *addr = linphone_core_interpret_url(LC, address.UTF8String);
    [LinphoneManager.instance call:addr];
    if (addr)
        linphone_address_destroy(addr);
}
- (void) acceptCall {
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (call) {
        [[LinphoneManager instance] acceptCall:call evenWithVideo:NO];
    }
}
- (void) declineCall {
    LinphoneCall *call = linphone_core_get_current_call(LC);
    linphone_core_terminate_call(LC, call);
}
- (void) sendDtmfWithKeyCode:(NSString*)keyCode {
    const char key = *[[keyCode substringToIndex:1] UTF8String];
    LinphoneCall *call = linphone_core_get_current_call(LC);
    linphone_call_send_dtmf(call, key);
}
- (int) getVolumeMax {
    // nothing
    return 0;
}
- (void) volume:(int)volume {
    // nothing
}
- (void) terminateCall {
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (linphone_core_is_in_conference(LC) ||										   // In conference
        (linphone_core_get_conference_size(LC) > 0) // Only one conf
        ) {
        linphone_core_terminate_conference(LC);
    } else if (call != NULL) { // In a call
        linphone_core_terminate_call(LC, call);
    } else {
        const MSList *calls = linphone_core_get_calls(LC);
        if (ms_list_size(calls) == 1) { // Only one call
            linphone_core_terminate_call(LC, (LinphoneCall *)(calls->data));
        }
    }
}
- (void) muteCall {
    linphone_core_enable_mic(LC, FALSE);
}
- (void) unmuteCall {
    linphone_core_enable_mic(LC, TRUE);
}
- (void) enableSpeaker {
    [[LinphoneManager instance] setSpeakerEnabled:TRUE];
}
- (void) disableSpeaker {
    [[LinphoneManager instance] setSpeakerEnabled:FALSE];
}
- (void) holdCall {
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (linphone_core_is_in_conference(LC) ||										   // In conference
        (linphone_core_get_conference_size(LC) > 0) // Only one conf
        ) {
        linphone_core_leave_conference(LC);
    } else if (call != NULL) { // In a call
        linphone_core_pause_call(LC, call);
    } else {
        const MSList *calls = linphone_core_get_calls(LC);
        if (ms_list_size(calls) == 1) { // Only one call
            linphone_core_pause_call(LC, (LinphoneCall *)(calls->data));
        }
    }
}
- (void) unholdCall {
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (linphone_core_is_in_conference(LC) ||										   // In conference
        (linphone_core_get_conference_size(LC) > 0) // Only one conf
        ) {
        linphone_core_enter_conference(LC);
    } else if (call != NULL) { // In a call
        linphone_core_resume_call(LC, call);
    } else {
        const MSList *calls = linphone_core_get_calls(LC);
        if (ms_list_size(calls) == 1) { // Only one call
            linphone_core_resume_call(LC, (LinphoneCall *)(calls->data));
        }
    }
}

- (NSDictionary*) getMissedCallLogs {
    const MSList *logs = linphone_core_get_call_logs([LinphoneManager getLc]);
    NSMutableDictionary *sections = [NSMutableDictionary dictionary];
    while (logs != NULL) {
        LinphoneCallLog *log = (LinphoneCallLog *)logs->data;
        if (linphone_call_log_get_status(log) == LinphoneCallMissed) {
            NSDate *startDate = [LinphoneHelper dateAtBeginningOfDayForDate:[NSDate
                                                              dateWithTimeIntervalSince1970:linphone_call_log_get_start_date(log)]];
            NSMutableArray *eventsOnThisDay = [sections objectForKey:startDate];
            if (eventsOnThisDay == nil) {
                eventsOnThisDay = [NSMutableArray array];
                [sections setObject:eventsOnThisDay forKey:startDate];
            }
            
            linphone_call_log_set_user_data(log, NULL);
            
            // if this contact was already the previous entry, do not add it twice
            LinphoneCallLog *prev = [eventsOnThisDay lastObject] ? [[eventsOnThisDay lastObject] pointerValue] : NULL;
            if (prev && linphone_address_weak_equal(linphone_call_log_get_remote_address(prev),
                                                    linphone_call_log_get_remote_address(log))) {
                MSList *list = linphone_call_log_get_user_data(prev);
                list = ms_list_append(list, log);
                linphone_call_log_set_user_data(prev, list);
            } else {
                //                [eventsOnThisDay addObject:[NSValue valueWithPointer:linphone_call_log_ref(log)]];
//                TTCallLog* ttCallLog = [[TTCallLog alloc] init];
//                [ttCallLog parseLinphoneCallLog:log];
//                [eventsOnThisDay addObject:ttCallLog];
            }
        }
        logs = ms_list_next(logs);
    }
    return sections;
}
- (NSDictionary*) getAllCallLogs {
    return nil;
}
- (void) deleteAllCallLogs {
    
}
- (void) startListener {
    // nothing
}

// --------------------------------------------------------------------
+ (NSMutableArray*) findAuthIndexOf:(NSString*)username domain:(NSString*)domain {
    LinphoneCore *lc = [LinphoneManager getLc];
    const MSList *accountList = linphone_core_get_auth_info_list(lc);
    int nbAccounts = ms_list_size(accountList);
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    for (int index = 0; index < nbAccounts; index++)
    {
        LinphoneAuthInfo* authInfo = ms_list_nth_data(accountList, index);
        
        const char *accountUsername = linphone_auth_info_get_username(authInfo);
        const char *accountDomain = linphone_auth_info_get_domain(authInfo);
        NSString *identity = [NSString stringWithFormat:@"%s@%s", accountUsername, accountDomain];
        NSString *sipAddress = [NSString stringWithFormat:@"%@@%@", username, domain];
        if ([identity isEqualToString:sipAddress]) {
            [indexes addObject:[NSNumber numberWithInteger:index]];
        }
    }
    return indexes;
}
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    NSDateComponents *dateComps =
    [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:inputDate];
    
    dateComps.hour = dateComps.minute = dateComps.second = 0;
    return [calendar dateFromComponents:dateComps];
}

@end
